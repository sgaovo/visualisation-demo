import os
import dateutil as du
import datetime as dt
import base64
import pandas as pd
import numpy as np
import itertools
import json

import datetime
import plotly.graph_objs as go
import numpy as np
import dash_core_components as dcc
import dash_html_components as html
from dash import Dash
from dash.dependencies import Input, Output

import plotly.io as pio
from plotly.io import write_image
import plotly.express as px
from plotly import graph_objs as go
from plotly.subplots import make_subplots
import sd_material_ui

def calendar_heatmap(data,
                    month_lines: bool = False,
                    fig = None,
                    row: int = None):
    
    '''
    data: numpy timeseries dataframe (with data.index a list of datetime and data.values a set of z values)
    fig: pass a figure to add this heatmap to as a subfigure, or None to create a new figure
    row: if passing a figure, the row which we're adding to
    '''
    dates = data.index
    values = data.values

    d1 = dates[0]
  
    month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    month_days =   [31,    28,    31,     30,    31,     30,    31,    31,    30,    31,    30,    31]

    # shift to take into account starting date
    def shift(l,n):
        return list(itertools.islice(itertools.cycle(l),n,n+len(l)))

    offset = int(d1.strftime('%m'))
    month_names = shift(month_names,offset)
    month_days = shift(month_days,offset)

    # calculate x label positions (month names) 
    month_positions = (np.cumsum(month_days) - 20)/7

    # calculate y values (week day)
    y_position_of_date = [i.weekday() for i in dates] 

    # calculate the x values - from the week number of each date
    starting_year = int(d1.strftime('%Y'))
    # week_numbers_absolute =  [int(i.strftime('%U')) + 52 * (int(i.strftime('%Y')) - starting_year) for i in dates]
    week_numbers_absolute = [(int(i.strftime('%W')) if not (int(i.strftime('%W')) == 1 and i.month == 12) else 53) + 52 * (int(i.strftime('%Y')) - starting_year) for i in dates]
    x_positions_of_date = [i - min(week_numbers_absolute) for i in week_numbers_absolute]

    # text = [str(i) for i in dates]
    text = ["{}: {} anomalies".format(date.strftime("%A, %d. %B %Y"), int(count)) for (date, count) in zip(dates, values)]
    colorscale=[[False, '#B7E0FF'], [True, '#006CEB']]
    
    data = [
        go.Heatmap(
            x=x_positions_of_date,
            y=y_position_of_date,
            z=values,
            text=text,
            hoverinfo='text',
            xgap=3, # make it look like a grid
            ygap=3, # make it look like a grid
            showscale=False,
            colorscale=colorscale
        )
    ]
    
    # display a boundary lines for each months
    if month_lines:
        kwargs = dict(
            mode='lines',
            line=dict(
                color='#9e9e9e',
                width=1
            ),
            hoverinfo='skip'
            
        )
        for date, dow, wkn in zip(dates,
                                  y_position_of_date,
                                  x_positions_of_date):
            if date.day == 1:
                data += [
                    go.Scatter(
                        x=[wkn-.5, wkn-.5],
                        y=[dow-.5, 6.5],
                        **kwargs
                    )
                ]
                if dow:
                    data += [
                    go.Scatter(
                        x=[wkn-.5, wkn+.5],
                        y=[dow-.5, dow - .5],
                        **kwargs
                    ),
                    go.Scatter(
                        x=[wkn+.5, wkn+.5],
                        y=[dow-.5, -.5],
                        **kwargs
                    )
                ]
                    
                    
    layout = go.Layout(
        title=None,
        height=250,
        yaxis=dict(
            showline=False, showgrid=False, zeroline=False,
            tickmode='array',
            ticktext=['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            tickvals=[0, 1, 2, 3, 4, 5, 6],
            autorange="reversed"
        ),
        xaxis=dict(
            side='top',
            showline=False, showgrid=False, zeroline=False,
            tickmode='array',
            ticktext=month_names,
            tickvals=month_positions
        ),
        # font={'size':10, 'color':'#9e9e9e'},
        font_family="Roboto, Droid Sans, Arial",
        font_color="#636d73",
        font_size=10,
        plot_bgcolor=('#fff'),
        margin = dict(t=40),
        showlegend=False
    )
    
    if fig is None:
        fig = go.Figure(data=data, layout=layout)
    else:
        fig.add_traces(data, rows=[(row+1)]*len(data), cols=[1]*len(data))
        fig.update_layout(layout)
        fig.update_xaxes(layout['xaxis'])
        fig.update_yaxes(layout['yaxis'])
    
    fig.update_layout(clickmode='event+select') 
    
    return fig



# data
end = du.parser.parse('23/05/20')
# end = dt.datetime.today()
start =  end -  dt.timedelta(days=364)
index = pd.date_range(start=start, end=end, freq='D')
# data = pd.Series(np.random.randint(0,4,size=(len(index))), index=index)
data = pd.Series(np.cumsum(np.ones(len(index))), index=index)
fig = calendar_heatmap(data)


app = Dash(
    __name__,
)

styles = {
    'pre': {
        'border': 'thin lightgrey solid',
        'overflowX': 'scroll'
    }
}

app.layout = html.Div([
    html.Div(className='row', children=[        
        dcc.Graph( 
            id='main-graph1',
            figure = fig,
            config = {'displaylogo': False, 'displayModeBar': False},
        )
    ]),
    html.Div(className='row', children=[
        html.Pre(id='detailsTable', style=styles['pre'])
    ])
])

@app.callback(
    Output('detailsTable', 'children'),
    Input('main-graph1', 'clickData'))
def display_click_data(clickData):
    return json.dumps(clickData, indent=2)

if __name__ == '__main__':
    app.run_server(host='localhost', port=8052, debug=True)